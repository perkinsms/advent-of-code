import re


cube_limits = {
        'blue': 14,
        'red' : 12,
        'green': 13,
        }

def score_line(text):
    title, rest = text.strip().split(':')
    score = int(title.strip().split(' ')[1])
    for game in rest.strip().split(';'):
        for trial in game.strip().split(','):
            number,color = trial.strip().split(' ')
            if int(number) > cube_limits[color]:
                return 0
    return score


def line_power(text):
    title,rest = text.strip().split(':')
    colors = {'red': 0,
              'blue': 0,
              'green': 0,
              }
    for game in rest.strip().split(';'):
        for trial in game.strip().split(','):
            count, color = trial.strip().split(' ')
            colors[color] = max([int(count), colors[color]])
    return colors['red'] * colors['blue'] * colors['green']
    

with open('input.txt') as f:
    print(sum(score_line(line) for line in f))
    f.seek(0)
    print(sum(line_power(line) for line in f))
