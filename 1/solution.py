import re


numbers = {'one': 1,
          'two': 2,
          'three': 3,
          'four': 4,
          'five': 5,
          'six': 6,
          'seven': 7,
          'eight': 8,
          'nine': 9,
          'zero': 0,
          }

pattern = '.*?(\d|one|two|three|four|five|six|seven|eight|nine|zero).*(\d|one|two|three|four|five|six|seven|eight|nine|zero).*?'
pattern2 = '.*(\d|one|two|three|four|five|six|seven|eight|nine|zero).*'

def number_replace(text):
    if text in numbers:
        return numbers[text]
    else:
        return text


with open('input.txt') as f:
    total = 0 
    for line in f:
        print(line.strip())
        if m := re.match(pattern, line.strip()):
            x = number_replace(m.group(1))
            y = number_replace(m.group(2))
            try:
                print(f'{x}{y}')
                number = int(f'{x}{y}')
                total += number
            except ValueError:
                print("Error, not a number")
        elif m:= re.match(pattern2, line.strip()):
            try:
                x = number_replace(m.group(1))
                print(f'{x}{x}')
                number = int(f'{x}{x}')
                total += number
            except ValueError:
                print("Error not a number")
        else:
            print("error no regexp match")

    print(total)




